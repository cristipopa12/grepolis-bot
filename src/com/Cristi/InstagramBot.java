package com.Cristi;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import sun.awt.windows.ThemeReader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InstagramBot {

    String username;    // account username
    String password;    // account password
    WebDriver driver;   // web driver

    // Constructor
    public InstagramBot(String username, String password) {
        this.username = username;
        this.password = password;
        System.setProperty("webdriver.gecko.driver", "E:\\Gecko\\geckodriver.exe");
        this.driver = new FirefoxDriver();
    }

    // Login function
    public void login() throws InterruptedException {

        driver.get("https://www.instagram.com/");
        Thread.sleep(2000);

        WebElement loginButton = ((FirefoxDriver) driver).findElementByXPath("//a[@href='/accounts/login/?source=auth_switcher']");
        loginButton.click();
        Thread.sleep(2000);

        WebElement user_name_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='username']");
        user_name_elem.clear();
        user_name_elem.sendKeys(username);
        WebElement password_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='password']");
        password_elem.clear();
        password_elem.sendKeys(password);
        password_elem.sendKeys(Keys.RETURN);
        Thread.sleep(2000);
    }


    public void like_photo() throws InterruptedException {

        // List of tags where to like photos
        // Search them one after another
        List<String> tagList = new ArrayList<>();
        tagList.add("nature");
        tagList.add("photooftheday");
        tagList.add("architecture");
        tagList.add("portrait");
        tagList.add("art");
        tagList.add("creative");

        int time_difference;
        // Get the hour as INT of current time
        int hour_current;
        // Get the minute as INT of current time
        int minute_current;
        // Get the second as INT of current time
        int seconds_current;

        // Get the hour as INT when the photo was uploaded
        int hour_photo;
        // Get the minute as INT when the photo was uploaded
        int minute_photo;
        // Get the second as INT when the photo was uploaded
        int seconds_photo;

        int photoIndex;


        // Iterating through all the tags and entering each one to perform the liking method
        for (String iterator:tagList) {
            driver.get("https://www.instagram.com/explore/tags/" + iterator + "/");
            Thread.sleep(2000);

            // When finding a tag page, we scoll down 5 times to display more images
            JavascriptExecutor js = (JavascriptExecutor) driver;
            for (int i = 0; i <5; i++) {
                // the scoll function
                js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
                Thread.sleep(1000);
            }


            // Get the current date and time to compare with the photo upload time
            String dateAsString;
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            DateTimeFormatter stf = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            // Match the format of the current date with the format of Instagram picture date
            dateAsString = dtf.format(now).toString() + "T" + stf.format(now).toString() + ".000Z";
            System.out.println(dateAsString);


            // Get the hour as INT of current time
            hour_current = (dateAsString.charAt(11) - '0')*10 + dateAsString.charAt(12) - '0';
            // Get the minute as INT of current time
            minute_current = (dateAsString.charAt(14) - '0')*10 + dateAsString.charAt(15) - '0';
            // Get the second as INT of current time
            seconds_current = (dateAsString.charAt(17) - '0')*10 + dateAsString.charAt(18) - '0';


            photoIndex = 1; // we have 6 recommended photos and I want to skip them
            // Put each photo url in a string list - we search by html tag <a>
            List<String> pic_hrefs = new LinkedList<>();
            List<WebElement> href = ((FirefoxDriver) driver).findElementsByTagName("a");
            String picturePrefix = "https://www.instagram.com/p/";
            for (WebElement element: href) {
                if(photoIndex <= 6){
                    photoIndex ++;
                }
                else{
                    if(element.getAttribute("href").toLowerCase().contains(picturePrefix)){
                        pic_hrefs.add(element.getAttribute("href"));
                        //System.out.println(element.getAttribute("href"));
                        photoIndex++;
                    }
                }

            }


            String time_value;      // The date and time when the current photo was uploaded

            // For each picture found before, access it's link
            for (String element:pic_hrefs) {
                driver.get(element);
                Thread.sleep(2000);

                // Get the photo upload date and time
                WebElement elapsed_time = ((FirefoxDriver) driver).findElementByXPath("//time[@class=\"_1o9PC Nzb55\"]");
                time_value = elapsed_time.getAttribute("datetime");
                WebElement like_button = ((FirefoxDriver) driver).findElementByXPath("//button[@class=\"wpO6b \"]");

                // If the photo was not posted in the current day, the bot will skip it
                boolean same_day = true;
                for (int i = 0; i < 9; i++) {
                    if(time_value.charAt(i) != dateAsString.charAt(i)){
                        same_day = false;
                        break;
                    }
                }

                if(same_day == true){


                    // Check if the photo was uploaded in the last 60 seconds
                    // If so then click the like button, else move to the next one
                    // First calculate the difference of seconds between the current time and the photo upload time

                    // Get the hour as INT when the photo was uploaded
                    hour_photo = (time_value.charAt(11) - '0')*10 + time_value.charAt(12) - '0';
                    // Get the minute as INT when the photo was uploaded
                    minute_photo = (time_value.charAt(14) - '0')*10 + time_value.charAt(15) - '0';
                    // Get the second as INT when the photo was uploaded
                    seconds_photo = (time_value.charAt(17) - '0')*10 + time_value.charAt(18) - '0';


                    time_difference = (hour_current - 2)*60*60 - hour_photo*60*60 + minute_current*60 - minute_photo*60 + seconds_current - seconds_photo;
                    if(time_difference < 60){
                        like_button.click();
                        Thread.sleep(1000);
                        System.out.println("Bot liked a photo posted " + time_difference + " seconds ago.");
                    }
                }

            }
        }

    }

    // https://www.instagram.com/explore/tags/nature/

}
