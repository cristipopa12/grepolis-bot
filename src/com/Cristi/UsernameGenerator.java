package com.Cristi;

import java.util.Random;

public class UsernameGenerator {

    private static final int MIN_LENGHT = 12;
    private static final int MAX_LENGHT = 20;

    // function to generate a random string of length n
    public String generateUsername()
    {

        // chose a Character random from this String
        String AlphaNumericString ="0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        int stringLenght = usernameLenght(MIN_LENGHT,MAX_LENGHT);

        // create StringBuffer size of AlphaNumericString
        StringBuilder username = new StringBuilder(stringLenght);

        for (int i = 0; i < stringLenght; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of username
            username.append(AlphaNumericString
                    .charAt(index));
        }

        return username.toString();
    }

    public int usernameLenght(int min, int max){
        Random rand = new Random();
        int lenght = rand.nextInt(max - min +1) +min;
        return lenght;
    }

}
