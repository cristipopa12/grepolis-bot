package com.Cristi;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import sun.awt.windows.ThemeReader;


public class GrepolisBot {

    private static final int ZEUS = 1;
    private static final int POSEIDON = 2;
    private static final int HADES = 3;
    private static final int ARTEMIS = 4;
    private static final String WORLD_NAME = "HELORUS";

    WebDriver driver;   // web driver

    public GrepolisBot() {
        System.setProperty("webdriver.gecko.driver", "E:\\Gecko\\geckodriver.exe");
        this.driver = new FirefoxDriver();
    }


    public void login(String username, String password) throws InterruptedException {

        // Get on the website
        driver.get("https://ro.grepolis.com/");
        Thread.sleep(2000);

        // Write the username and password
        WebElement user_name_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='login[userid]']");
        user_name_elem.clear();
        user_name_elem.sendKeys(username);
        WebElement password_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='login[password]']");
        password_elem.clear();
        password_elem.sendKeys(password);
        password_elem.sendKeys(Keys.RETURN);
        Thread.sleep(2000);

        selectWorld();

    }


    public void register(String link, String username, String password) throws InterruptedException {

        driver.get(link);
        Thread.sleep(2000);

        WebElement user_name_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='name']");
        user_name_elem.clear();
        user_name_elem.sendKeys(username);
        Thread.sleep(1000);
        WebElement password_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='password']");
        password_elem.clear();
        password_elem.sendKeys(password);
        Thread.sleep(1000);
        WebElement repeat_password_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='password_confirmation']");
        repeat_password_elem.clear();
        repeat_password_elem.sendKeys(password);
        Thread.sleep(1000);
        WebElement email_elem = ((FirefoxDriver) driver).findElementByXPath("//input[@name='email']");
        email_elem.clear();
        email_elem.sendKeys(username+"@yahoo.com");
        WebElement accept_terms = ((FirefoxDriver) driver).findElementByXPath("//input[@name='terms_accepted']");
        accept_terms.click();
        Thread.sleep(1000);
        WebElement register_button = ((FirefoxDriver) driver).findElementByClassName("middle");
        register_button.click();
        Thread.sleep(3000);

    }

    public void register_NO_link(String username, String password) throws InterruptedException {

        driver.get("https://ro.grepolis.com/");

        WebElement user_name_elem = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"registration_nickname\"]");
        user_name_elem.clear();
        user_name_elem.sendKeys(username);
        Thread.sleep(1000);
        WebElement password_elem = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"registration_password\"]");
        password_elem.clear();
        password_elem.sendKeys(password);
        Thread.sleep(1000);
        WebElement email_elem = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"registration_email\"]");
        email_elem.clear();
        email_elem.sendKeys(username+"@yahoo.com");
        WebElement accept_terms = ((FirefoxDriver) driver).findElementByCssSelector(".terms > label:nth-child(1)");
        accept_terms.click();
        Thread.sleep(1000);
        WebElement register_button = ((FirefoxDriver) driver).findElementByCssSelector("#registration_register");
        register_button.click();
        Thread.sleep(3000);


    }

    public void selectWorld() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if (driver.findElements(new By.ByXPath("//li[@data-worldname=\"DYME\"]")).size() != 0) {

                // Select the world
                WebElement world_selection = ((FirefoxDriver) driver).findElementByXPath("//li[@data-worldname=\"DYME\"]");
                world_selection.click();
                System.out.println(world_selection.toString());
                Thread.sleep(2500);
                break;
            } else {
                Thread.sleep(1000);
            }

        }
    }


    public void get_bonus() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector("#daily_login_icon")).size() != 0){

                //WebElement favor_reward = ((FirefoxDriver) driver).findElementByXPath("//div[@class = 'res favor']");
                WebElement favor_reward = ((FirefoxDriver) driver).findElementByCssSelector("#daily_login_icon");
                favor_reward.click();
                Thread.sleep(1000);
                break;
            }

            else {
                Thread.sleep(1000);
            }
        }

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector("div.favor:nth-child(1)")).size() != 0){

                WebElement accept = ((FirefoxDriver) driver).findElementByCssSelector("div.favor:nth-child(1)");
                accept.click();
                Thread.sleep(1000);
                break;
            }

            else {
                Thread.sleep(600);
            }
        }

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector(".btn_confirm > div:nth-child(3)")).size() != 0){

                WebElement accept = ((FirefoxDriver) driver).findElementByCssSelector(".btn_confirm > div:nth-child(3)");
                accept.click();
                Thread.sleep(1000);
                break;
            }

            else {
                Thread.sleep(600);
            }
        }

    }

    public void build_city() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector("#btn_start_tutorial > div:nth-child(3)")).size() != 0){

                WebElement build_town_stuff = ((FirefoxDriver) driver).findElementByCssSelector("#btn_start_tutorial > div:nth-child(3)");
                build_town_stuff.click();
                Thread.sleep(1000);
                break;
            }

            else {
                Thread.sleep(1000);
            }
        }


    }

    public void close_promo() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector(".btn_wnd")).size() != 0){
                Thread.sleep(1000);
                WebElement close = ((FirefoxDriver) driver).findElementByCssSelector(".btn_wnd");
                close.click();

                //Actions action = new Actions(driver);
                //action.sendKeys(Keys.ESCAPE);

                break;
            }

            else {
                Thread.sleep(1100);
            }
        }

    }



    public void enter_building_mode() throws InterruptedException {

        WebElement build_hammer = ((FirefoxDriver) driver).findElementByXPath("//div[@class = 'construction_queue_sprite queue_button-idle queue_button btn_construction_mode js-tutorial-btn-construction-mode']");
        build_hammer.click();
        Thread.sleep(1500);

    }

    public void upgrade_wood() throws InterruptedException {
        WebElement wood = ((FirefoxDriver) driver).findElementByXPath("/html/body/div[1]/div[23]/div/div/div[2]/div/div[3]/div[2]/div/div/div[1]");
        wood.click();
        Thread.sleep(2000);
    }

    public void upgrade_silver() throws InterruptedException {
        WebElement silver = ((FirefoxDriver) driver).findElementByXPath("/html/body/div[1]/div[23]/div/div/div[2]/div/div[5]/div[2]/div/div/div[1]");
        silver.click();
        Thread.sleep(2000);
    }

    public void upgrade_stone() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector("div.city_overview_overlay:nth-child(4) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)")).size() != 0){

                WebElement stone = ((FirefoxDriver) driver).findElementByCssSelector("div.city_overview_overlay:nth-child(4) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
                stone.click();
                Thread.sleep(1000);

                break;
            }

            else {
                Thread.sleep(1000);
            }
        }

    }

    public String getInvitationLink() throws InterruptedException {

        WebElement button1 = ((FirefoxDriver) driver).findElementByCssSelector(".invite_friends > span:nth-child(1) > span:nth-child(2) > span:nth-child(1)");
        button1.click();
        Thread.sleep(1000);
        WebElement button2 = ((FirefoxDriver) driver).findElementByCssSelector(".btn_invite_url > div:nth-child(3)");
        button2.click();
        Thread.sleep(1000);
        WebElement button3 = ((FirefoxDriver) driver).findElementByCssSelector("a.button > span:nth-child(1) > span:nth-child(1) > span:nth-child(1)");
        button3.click();
        Thread.sleep(1000);
        WebElement link = ((FirefoxDriver) driver).findElementByCssSelector(".grepo_input > span:nth-child(1) > span:nth-child(1) > input:nth-child(1)");
        link.getAttribute("value");
        Thread.sleep(1000);
        return link.getAttribute("value");
    }

    public void upgrade_temple() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector("div.city_overview_overlay:nth-child(7) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)")).size() != 0){

                WebElement temple = ((FirefoxDriver) driver).findElementByCssSelector("div.city_overview_overlay:nth-child(7) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
                temple.click();
                Thread.sleep(2000);
                break;
            }

            else {
                Thread.sleep(1000);
            }
        }
    }

    public void upgrade_now() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector(".btn_time_reduction")).size() != 0){

                WebElement instant = ((FirefoxDriver) driver).findElementByCssSelector(".btn_time_reduction");

                instant.click();
                Thread.sleep(2000);
                break;
            }

            else {
                Thread.sleep(2000);
            }
        }


    }

    public void god_select(int god_number) throws InterruptedException {

        WebElement god_pannel = ((FirefoxDriver) driver).findElementByCssSelector(".gods_container");

        god_pannel.click();
        Thread.sleep(1500);

        switch (god_number){
            case ZEUS:
                WebElement god1 = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"temple_zeus\"]");
                god1.click();
                Thread.sleep(1000);

                WebElement zeus = ((FirefoxDriver) driver).findElementByCssSelector(".btn_change_god");
                zeus.click();
                Thread.sleep(1000);
                break;

            case POSEIDON:
                WebElement god2 = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"temple_poseidon\"]");
                god2.click();
                Thread.sleep(1000);

                WebElement poseidon = ((FirefoxDriver) driver).findElementByCssSelector(".btn_change_god");
                poseidon.click();
                Thread.sleep(1000);
                break;

            case HADES:
                WebElement god3 = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"temple_hades\"]");
                god3.click();
                Thread.sleep(1000);

                WebElement hades = ((FirefoxDriver) driver).findElementByCssSelector(".btn_change_god");

                hades.click();
                Thread.sleep(1000);
                break;

            case ARTEMIS:
                WebElement god4 = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"temple_artemis\"]");
                god4.click();
                Thread.sleep(1000);

                WebElement artemis = ((FirefoxDriver) driver).findElementByCssSelector(".btn_change_god");
                artemis.click();
                Thread.sleep(1000);
                break;

        }
    }

    public void logout() throws InterruptedException {

        int count = 5;

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector(".btn_logout > div:nth-child(1)")).size() != 0){

                WebElement logout = ((FirefoxDriver) driver).findElementByCssSelector(".btn_logout > div:nth-child(1)");
                logout.click();
                Thread.sleep(2000);
                break;
            }

            else {
                Thread.sleep(2000);
            }
        }

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector("#logout > span:nth-child(3) > a:nth-child(1)")).size() != 0){

                WebElement start_page = ((FirefoxDriver) driver).findElementByCssSelector("#logout > span:nth-child(3) > a:nth-child(1)");
                start_page.click();
                Thread.sleep(2000);
                break;
            }

            else {
                Thread.sleep(2000);
            }
        }

        for (int i = 0; i < count; i++) {
            if ( driver.findElements(new By.ByCssSelector(".logout_button")).size() != 0){

                WebElement disconnect = ((FirefoxDriver) driver).findElementByCssSelector(".logout_button");
                disconnect.click();
                Thread.sleep(1000);
                break;
            }

            else {
                Thread.sleep(2000);
            }
        }

    }

    public void build_new_account(int god) throws InterruptedException {

        build_city();

        WebElement minimize = ((FirefoxDriver) driver).findElementByCssSelector("div.btn_wnd:nth-child(1)");
        minimize.click();
        Thread.sleep(1000);

        enter_building_mode();
        upgrade_stone();
        upgrade_now();
        upgrade_temple();
        upgrade_now();
        upgrade_temple();
        god_select(god);

    }

    public boolean isConquered(){
        return true;
    }

    public void sendResources(int god) throws InterruptedException {

        for (int i = 0; i < 25; i++) {

            switch (god){
                case ZEUS:
                    WebElement zeus = ((FirefoxDriver) driver).findElementByCssSelector("div.js-god-box:nth-child(2) > div:nth-child(4) > div:nth-child(1)");
                    zeus.click();
                    Thread.sleep(200);
                    break;
                case POSEIDON:
                    WebElement poseidon = ((FirefoxDriver) driver).findElementByCssSelector("div.js-god-box:nth-child(3) > div:nth-child(4) > div:nth-child(1)");
                    poseidon.click();
                    Thread.sleep(200);
                    break;
                case HADES:
                    WebElement hades = ((FirefoxDriver) driver).findElementByCssSelector("div.underworld_treasures:nth-child(2)");
                    hades.click();
                    Thread.sleep(200);
                    break;
                case ARTEMIS:
                    WebElement artemis = ((FirefoxDriver) driver).findElementByCssSelector("div.js-god-box:nth-child(7) > div:nth-child(4) > div:nth-child(1)");
                    artemis.click();
                    Thread.sleep(200);
                    break;
                case 5:
                    WebElement hera = ((FirefoxDriver) driver).findElementByCssSelector("div.js-god-box:nth-child(4) > div:nth-child(4) > div:nth-child(1)");
                    hera.click();
                    Thread.sleep(20);
                    break;
            }
        }
    }

    public void findPlayer(String name, int city_number) throws InterruptedException {

        WebElement leaderboard = ((FirefoxDriver) driver).findElementByCssSelector(".ranking > span:nth-child(1) > span:nth-child(2) > span:nth-child(1)");
        leaderboard.click();
        Thread.sleep(1000);

        WebElement playerName = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"player_name\"]");
        playerName.clear();
        playerName.sendKeys(name);
        Thread.sleep(1000);

        WebElement search = ((FirefoxDriver) driver).findElementByCssSelector("a.button > span:nth-child(1) > span:nth-child(1) > span:nth-child(1)");
        search.click();
        Thread.sleep(1500);

        WebElement player = ((FirefoxDriver) driver).findElementByCssSelector(".gp_player_link");
        player.click();
        Thread.sleep(1000);

        WebElement city = ((FirefoxDriver) driver).findElementByCssSelector("#player_towns > div:nth-child(1) > ul:nth-child(10) > li:nth-child(1) > a:nth-child(2)");
        city.click();
        Thread.sleep(1000);

        WebElement spells = ((FirefoxDriver) driver).findElementByXPath("//*[@id=\"god\"]");
        spells.click();
        Thread.sleep(1000);

    }

    public void changeWorld() throws InterruptedException {

        WebElement other_world = ((FirefoxDriver) driver).findElementByCssSelector(".other_worlds > div:nth-child(1)");
        other_world.click();
        Thread.sleep(2000);

        WebElement new_world = ((FirefoxDriver) driver).findElementByCssSelector("li.world:nth-child(1) > div:nth-child(4) > a:nth-child(1) > span:nth-child(1) > span:nth-child(1)");
        new_world.click();
        Thread.sleep(2000);

        WebElement next = ((FirefoxDriver) driver).findElementByCssSelector("li.world:nth-child(1) > div:nth-child(5) > div:nth-child(5) > div:nth-child(2) > a:nth-child(1) > span:nth-child(1) > span:nth-child(1) > span:nth-child(1)");
        next.click();
        Thread.sleep(2000);

        WebElement map_position = ((FirefoxDriver) driver).findElementByCssSelector("#nw");
        map_position.click();
        Thread.sleep(1000);

        WebElement accept = ((FirefoxDriver) driver).findElementByCssSelector(".middle");
        accept.click();
        Thread.sleep(500);

        Thread.sleep(2500);
    }

    public void stage1_TO_stage2(String username, String password) throws InterruptedException {

            driver.get("https://ro0.grepolis.com/invite-1025758-ro68?invitation_id=513242&invitation_mac=5dd87755&ref=player_invite_linkrl");
            Thread.sleep(2000);

            WebElement user_name_elem = ((FirefoxDriver) driver).findElementByCssSelector("div.loginform-inner-wrap:nth-child(1) > input:nth-child(2)");
            user_name_elem.clear();
            user_name_elem.sendKeys(username);
            Thread.sleep(1000);
            WebElement password_elem = ((FirefoxDriver) driver).findElementByCssSelector("div.loginform-inner-wrap:nth-child(2) > input:nth-child(2)");
            password_elem.clear();
            password_elem.sendKeys(password);
            Thread.sleep(1000);
            WebElement register_button = ((FirefoxDriver) driver).findElementByCssSelector("#login_button > a:nth-child(1) > span:nth-child(1) > span:nth-child(1) > span:nth-child(1)");
            register_button.click();
            Thread.sleep(3000);

    }

}


